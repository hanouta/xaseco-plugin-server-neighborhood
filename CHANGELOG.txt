CHANGES 1.4.8
~~~~~~~~~~~~~
 - Changes:
   - none

 - Changes in the server_neighborhood.xml:
   - Changed all <urls>'s to ManiaCDN

 - Bugfix:
   - none



CHANGES 1.4.7
~~~~~~~~~~~~~
 - Changes:
   - This release require min. XAseco/1.16!

 - Changes in the server_neighborhood.xml:
   - Added <widget><timer_bar>*
   - Changed all <urls>*

 - Bugfixes:
   - In Gamemode "Stunts" the Score was not stored right
   - [PHP Notice] Undefined offset: [N] on... (thanks kremsy)



CHANGES 1.4.6
~~~~~~~~~~~~~
 - Changes:
   - This release require min. XAseco/1.15b!
   - Included in the ServerOverviewWindow the Totals over all Servers (thanks TheBigG)

 - Changes in the server_neighborhood.xml:
   - Added <widget><title_style> and <widget><title_substyle> (thanks tyjo)

 - Bugfixes:
   - none



CHANGES 1.4.5
~~~~~~~~~~~~~
 - NOTE:
   - This release require min. XAseco/1.14!
   - You need to update all Servers!

 - Changes:
   - Renamed 'serverneighborhood.xml' to 'server_neighborhood.xml'
   - Added new chat command "/neighborreload" to reload the 'server_neighborhood.xml' (thanks TheBigG)
   - Changed the max. amount of Neighbors from 100 to 56
   - Added Rotation of the Neighborhoods to save space at the Widget (thanks tyjo)
   - Added global Plugins Register Pool to enabling version check (for plugin.third-party-plugins-uptodate.php)
   - Added the possibility to change the Color and Opacity of the Window-Background (thanks TheBigG)
   - Added the possibility to enable/disable <server_neighbor>'s instead to remove them and add them again later
     (see <server_accounts><server_neighbor><enable> in serverneighborhood.xml at each neighbor)
   - Added a reload button in the ServerOverviewWindow
   - Added the new AddToFavorite button where the Players can easy add your Server into his/her Favorite (useful for new Trackmania Players) (thanks craigb456)

 - Changes in the server_neighborhood.xml:
   - Added <server_display_max> for the Rotation of the Neighborhoods
   - Renamed <colors><background> to <colors><server_background>
   - Added <colors><neighbor_background>
   - New urls for the <urls>*

 - Bugfix:
   - Now handle $P links too
   - At Gamemode 'Stunts' and a starting XAseco with connected Players results into "Trying to get property of non-object"
   - UTF-8 character in <widget><header_name> was not correct at the Header from the Widget (thanks TheBigG)



CHANGES 1.4.4
~~~~~~~~~~~~~
 - NOTE: You need to update all servers!
 - Added onPlayerConnect Event to display widget (thanks .anDy)
 - No longer convert stunt scores as times (thanks .anDy)
 - Nicemode available like Records Eyepiece (thanks .anDy)
 - Speed-up displaying of Widgets (thanks .anDy)
 - Added server gamemode to Server Neighborhood-Widget (thanks .anDy)
 - Can be configured for each gamemode now (widget, pos-x, pos-y) (thanks .anDy)
 - <login> of <server_neighbor> removed (thanks .anDy)
 - Displays the correct flags now (e.g. Romania) (thanks .anDy)
 - If a TMNF-Player wants to switch to a TMUF-Server, then the button is now disabled
 - Remove all links from ServerName or Player Nickname
 - Removed <widget><icon_position>, this are now set by an automatic


CHANGES 1.4.3
~~~~~~~~~~~~~
 - Speed-up the hidding at Scoretable
 - Changed the Layer (z-index) in the display port
 - Removing URLs und Links from Servername


CHANGES 1.4.2
~~~~~~~~~~~~~
 - Switched from event onEndRace to onEndRace1


CHANGES 1.4.1
~~~~~~~~~~~~~
 - Bugfix: A Server from 7500-50000 the widget says 75-50K, when it should say 7.5-50K (thanks Yorkshire)


CHANGES 1.4
~~~~~~~~~~~
 - Complete new layout of the Server-Overview (see Screenshots)
 - Included new Trackinfo-Window (see Screenshots)
 - Serverinfo XML-Files are changed, now storing more information about the current Challenge (you need to update all servers!)


CHANGES 1.3.2
~~~~~~~~~~~~~
 - New option in config: <widget><hidden> Useful if you want to write the XML-Files, but hide the Widget (e.g. on War-/Train-Servers)
 - In the XML-Files now storing <login> and <nation> for each Player too
 - The Server-Overview displays the Nation for each Player (Screenshot "Server-Overview" now outdated)


CHANGES 1.3.1
~~~~~~~~~~~~~
 - Bugfix: Toggle Widget hide/show and Map change fixed
 - Above Screeshots updated


CHANGES 1.3
~~~~~~~~~~~
 - Based upon Milenco�s changed version (mem leak free, thx milenco)
 - Include all changes from 1.2.6 till 1.2.8 of my port
 - Include the F7 toggle to hide/show the Widget like fufi.widgets
 - Screeshot does not show the actual ServerOverview (missed Ladder ranking and Player status, need to update  )


CHANGES 1.2.8
~~~~~~~~~~~~~
 - This release includes logging of memory usage. This information are written on every Challenge change and if the crashes with the memory_limit message, i need the last 10-20 entries for debug (post here or PM).
 - Memory leak fixed (thanks svens)
 - Header name option in config file (thanks milenco)
 - Replaced special characters in challenge-name (thanks milenco)
 - Default playercolor is now white instead of black (thanks milenco)


CHANGES 1.2.7
~~~~~~~~~~~~~
 - This release includes logging of memory usage. This information are written on every Challenge change and if the crashes with the memory_limit message, i need the last 10-20 entries for debug (post here or PM).
 - Include the display of LadderRanking and the status "Player" or "Spectator" for each Player in the Server-Overview (you need to upgrade all your Server to get this information)


CHANGES 1.2.6
~~~~~~~~~~~~~
 - Fixed bug where spectator-icon and padlock would be shown at the same time
 - Inluded the <force_spectator> option for each Server into the serverneighborhood.xml to set a related link


CHANGES 1.2.5
~~~~~~~~~~~~~
 - Inluded the <debug> option into the serverneighborhood.xml to disable warnings in logfiles
 - Fixed the replacement of <>&"' for Players
 - NOTE: Please set in your php.ini the memory_limit to min. 8M (default limit in PHP/5.2.13 is '128M' !)


CHANGES 1.2.4
~~~~~~~~~~~~~
 - Another Bugfix release
 - Fixed the replacement of <>&"' for Players and Servername


CHANGES 1.2.3
~~~~~~~~~~~~~
 - Another Bugfix release


CHANGES 1.2.2
~~~~~~~~~~~~~
 - Bugfix: Annotated things changed/integrated, thx �svens�!


CHANGES 1.2.1
~~~~~~~~~~~~~
 - Bugfix: Allowed memory size of 52428800 bytes exhausted (hopefully)


CHANGES 1.2
~~~~~~~~~~~
 - Bugfix: Sometimes did not open the Window after a click on a Server-Neighbor


CHANGES 1.1
~~~~~~~~~~~
 - Switched from TXT-Files to XML-Files
 - Included a Timestamp (which is used to hide a server from Widget with a configured last modified time <hide_server_last_modified>)
 - Removed <show_self> from serverneighborhood.xml


CHANGES 1.0
~~~~~~~~~~~
 - NOTE: If you install this version you need to install it on all your servers, otherwise the server will be hidden. The format of the ServerInfo-Files has changed!
 - New feature: If the Server has a password, then now a padlock is shown.
 - New feature: The ServerInfo-Files with the neighbors must not saved at same place (e.g. <storing_path>)
 - Removed <show_self> from serverneighborhood.xml
 - Bugfix: Sometimes a ServerInfo-File read fails and did not show the server


CHANGES 0.9.9
~~~~~~~~~~~~~
 - New feature: Storing the neighbors not only local, now it is possible to set <storing_path> to an FTP-Address.
